﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace RestConsumer.Utility
{
    /// <summary>
    /// Performs deserialization on response objects, and various other checks.
    /// </summary>
    internal static class JSONHelper
    {
        /// <summary>Performs the deserialisation of the supplied response content as T.</summary>
        /// <typeparam name="T">The supplied generic data type.</typeparam>
        /// <param name="content">The response content from a <see cref="HttpResponseMessage"/>.</param>
        internal async static Task<T> DeserializeResponseContent<T>(HttpContent content) where T : class
        {
            T deserializedValue = default(T);

            try
            {
                switch (content.Headers.ContentType.MediaType)
                {
                    case "application/json":
                        var jsonContent = await content.ReadAsStringAsync().ConfigureAwait(false);
                        deserializedValue = JsonConvert.DeserializeObject<T>(jsonContent);
                        break;

                    case "text/plain":
                        var plainTextContent = await content.ReadAsStringAsync().ConfigureAwait(false);
                        deserializedValue = plainTextContent as T;
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new HttpRequestException(ex.Message, ex);
            }

            return deserializedValue;
        }

        /// <summary>Checks if the supplied string is valid a JSON format.</summary>
        /// <param name="input">The supplied JSON string to check.</param>
        internal static bool IsValidJson(string input)
        {
            input = input.Trim();

            // If the container elements aren't JSON compliant, then let's fail.
            if (!(input.StartsWith("{") || input.StartsWith("[")) && (input.EndsWith("}") || input.EndsWith("]")))
                return false;

            try
            {
                var obj = JObject.Parse(input);

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
