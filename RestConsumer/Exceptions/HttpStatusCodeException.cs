﻿using System;
using System.Net;

namespace RestConsumer.Exceptions
{
    /// <summary>
    /// Model for an exception where a <see cref="HttpStatusCode"/> is required.
    /// </summary>
    public class HttpStatusCodeException : Exception
    {
        /// <summary>The HttpStatusCode for a server response.</summary>
        public HttpStatusCode HttpStatusCode { get; private set; }

        /// <summary>Creates an instance of <see cref="HttpStatusCodeException"/> with the supplied <paramref name="httpStatusCode"/>.</summary>
        /// <param name="httpStatusCode">The supplied <see cref="HttpStatusCode"/>.</param>
        public HttpStatusCodeException(HttpStatusCode httpStatusCode)
        {
            this.HttpStatusCode = httpStatusCode;
        }
    }
}
