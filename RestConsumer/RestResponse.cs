﻿using System;
using System.Net.Http;

namespace RestConsumer
{
    /// <summary>
    /// Model for the deserialized response data, and appropriate messages.
    /// </summary>
    /// <typeparam name="T">The expected type for the response to deserialize to.</typeparam>
    public class RestResponse<T> where T : class
    {
        /// <summary>The deserialized response content data as T.</summary>
        public T Data { get; private set; }

        /// <summary>The response message from the server.</summary>
        public HttpResponseMessage HttpResponseMessage { get; private set; }

        /// <summary>The exception (if appropriate).</summary>
        public Exception Exception { get; private set; }

        /// <summary>Creates an instance of <see cref="RestResponse{T}"/>.</summary>
        /// <param name="httpResponseMessage">The supplied <see cref="HttpResponseMessage"/>.</param>
        /// <param name="data">The supplied type to deserialize to.</param>
        /// <param name="ex">The (optional) exception used to provide more information to the caller.</param>
        public RestResponse(HttpResponseMessage httpResponseMessage, T data, Exception ex = null)
        {
            this.HttpResponseMessage = httpResponseMessage;
            this.Data = data;

            if (ex != null)
                this.Exception = ex;
        }
    }
}
