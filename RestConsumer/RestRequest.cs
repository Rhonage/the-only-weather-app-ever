﻿using System.Net.Http;

namespace RestConsumer
{
    /// <summary>
    /// Model for the request to the RESTful API.
    /// </summary>
    public class RestRequest
    {
        /// <summary>The HTTP Method to use for the request (GET, POST, PUT, DELETE)</summary>
        public HttpMethod Method { get; private set; }

        /// <summary>The string representation of the resource to access.</summary>
        public string RequestUri { get; private set; }

        private RestRequest()
        {
            this.Method = HttpMethod.Get;
        }

        /// <summary>Creates an instance of a <see cref="RestRequest"/> with the supplied <paramref name="requestUri"/>.</summary>
        /// <param name="requestUri">The supplied request URI.</param>
        public RestRequest(string requestUri) : this()
        {
            this.RequestUri = requestUri;
        }

        /// <summary>Creates an instance of <see cref="RestRequest"/> with the supplied <paramref name="requestUri"/> and <paramref name="method"/>.</summary>
        /// <param name="requestUri">The supplied request URI.</param>
        /// <param name="method">The supplied <see cref="HttpMethod"/> to perform on the REST call.</param>
        public RestRequest(string requestUri, HttpMethod method) : this(requestUri)
        {
            this.Method = method;
        }
    }
}
