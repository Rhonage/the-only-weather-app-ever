﻿using RestConsumer.Exceptions;
using RestConsumer.Extensions;
using RestConsumer.Utility;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace RestConsumer
{
    /// <summary>
    /// Base class for a REST Consumer. Generics are used so that this can handle any data communication between the application and the RESTful API.
    /// </summary>
    public abstract class ConsumerBase
    {
        /// <summary>The URI endpoint for the RESTful API call.</summary>
        protected abstract string BaseUri { get; }

        // Rohan's Note: I often like to lazily instantiate my variables with backing fields so I can act on the properties without having to worry about null checks.
        private HttpClient Client { get { return this._client ?? (this._client = new HttpClient() { BaseAddress = new Uri(this.BaseUri) }); } }
        private HttpClient _client;

        /// <summary>Executes an async request to the supplied <see cref="RestRequest"/> resource, and deserializes the response content to an object of T.</summary>
        /// <typeparam name="T">The type to deserialize to.</typeparam>
        /// <param name="request">The <see cref="RestRequest"/> (containing the Resource) to invoke.</param>
        protected async Task<RestResponse<T>> ExecuteAsync<T>(RestRequest request) where T : class
        {
            try
            {
                // Form the HttpRequestMessage by finding the full Uri, and setting the appropriate values.
                var httpRequestMessage = this.GetHttpRequestMessage(request);

                // Send the request, and retrieve HttpResponseMessage from server.
                var httpResponseMessage = await this.Client.SendAsync(httpRequestMessage).ConfigureAwait(false);
                httpResponseMessage.EnsureSuccessStatusCodeWithResult();

                // If all goes well, deserialize JSON (and other response types) to T and create our response object for our client consumers.
                var responseContent = await this.GetResponseContent<T>(httpResponseMessage).ConfigureAwait(false);

                return new RestResponse<T>(httpResponseMessage, responseContent);
            }
            catch (HttpRequestException ex)
            {
                return new RestResponse<T>(new HttpResponseMessage(HttpStatusCode.BadRequest), null, ex);
            }
            catch (HttpStatusCodeException ex)
            {
                return new RestResponse<T>(new HttpResponseMessage(ex.HttpStatusCode), null, ex);
            }
            catch (Exception ex)
            {
                return new RestResponse<T>(null, null, ex);
            }
        }

        private HttpRequestMessage GetHttpRequestMessage(RestRequest request)
        {
            // Begin building the HttpRequestMessage
            var httpRequestMessage = new HttpRequestMessage(request.Method, request.RequestUri);

            return httpRequestMessage;
        }

        private async Task<T> GetResponseContent<T>(HttpResponseMessage response) where T : class
        {
            if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
                return default(T);

            return await JSONHelper.DeserializeResponseContent<T>(response.Content).ConfigureAwait(false);
        }
    }
}
