﻿using RestConsumer.Exceptions;
using System.Net.Http;

namespace RestConsumer.Extensions
{
    /// <summary>
    /// Extension methods for HttpResponseMessages.
    /// </summary>
    public static class HttpResponseMessageExtensions
    {
        /// <summary>Ensure that the response received from the server was successful.</summary>
        /// <param name="response">The supplied <see cref="HttpResponseMessage"/>.</param>
        public static void EnsureSuccessStatusCodeWithResult(this HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
                return;

            var content = response.Content.ReadAsStringAsync().Result;

            if (response.Content != null)
                response.Content.Dispose();

            throw new HttpStatusCodeException(response.StatusCode);
        }
    }
}
