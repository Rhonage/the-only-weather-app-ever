﻿namespace TheOnlyWeatherAppEver.Tests
{
    /// <summary>
    /// Constant values for unit tests.
    /// </summary>
    internal static class Consts
    {
        /// <summary>City name for Timaru.</summary>
        internal const string CityName = "Timaru";

        /// <summary>City ID of Timaru.</summary>
        internal const string CityId = "2181133";

        /// <summary>Country code for New Zealand.</summary>
        internal const string CountryCode = "NZ";

        /// <summary>Latitude value for Timaru.</summary>
        internal const string Latitude = "-44.4";

        /// <summary>Longitude value for Timaru.</summary>
        internal const string Longitude = "171.25";

        /// <summary>Unit test category for Current Weather Tests.</summary>
        internal const string CategoryCurrentWeather = "OpenWeather_CurrentWeather";

        /// <summary>Unit test category for FreeGeoIPLocation Tests.</summary>
        internal const string CategoryFreeGeoIPLocation = "FreeGeoIP_Location";
    }
}
