﻿using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace TheOnlyWeatherAppEver.Tests
{
    /// <summary>
    /// Unit tests for retrieving device location by external IP address.
    /// </summary>
    [TestClass]
    public class FreeGeoIPTests
    {
        /// <summary>Tests that a location can be retrieved for the device's external IP.</summary>
        [TestMethod]
        [TestCategory(Consts.CategoryFreeGeoIPLocation)]
        public void Device_Location_Is_Found_Successfully()
        {
            var consumer = new TheOnlyWeatherAppEver.Consumers.FreeGeoIPConsumer();
            var ipGeoLocation = consumer.GetLocationData();

            Assert.IsNotNull(ipGeoLocation);
            Assert.IsNotNull(ipGeoLocation.IPAddress);
            Assert.IsNotNull(ipGeoLocation.City);
            Assert.IsNotNull(ipGeoLocation.CountryCode);
            Assert.IsNotNull(ipGeoLocation.Latitude);
            Assert.IsNotNull(ipGeoLocation.Longitude);
        }
    }
}
