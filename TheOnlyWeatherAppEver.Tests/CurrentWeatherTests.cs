﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TheOnlyWeatherAppEver.Tests
{
    /// <summary>
    /// Unit tests for retrieving current weather data for Timaru.
    /// </summary>
    /// <remarks>Having more time, I would battle test these a lot more (especially the assertations) - but for now I am just testing against my location in Timaru.</remarks>
    [TestClass]
    public class CurrentWeatherTests
    {
        /// <summary>Tests that the current weather can be retrieved by City ID.</summary>
        [TestMethod]
        [TestCategory(Consts.CategoryCurrentWeather)]
        public void Current_Weather_By_City_Id_Returns_Expected_Data()
        {
            var viewModel = new TheOnlyWeatherAppEver.ViewModels.SearchLocationViewModel();
            viewModel.CityIdInput = Consts.CityId;
            viewModel.GetLocationByCityIdCommand.Execute(null);

            this.AssertSuccessfulResponse(viewModel.Location);
        }

        /// <summary>Tests that the current weather can be retrieved by City Name.</summary>
        [TestMethod]
        [TestCategory(Consts.CategoryCurrentWeather)]
        public void Current_Weather_By_City_Name_Returns_Expected_Data()
        {
            var viewModel = new TheOnlyWeatherAppEver.ViewModels.SearchLocationViewModel();
            viewModel.CityNameInput = Consts.CityName;
            viewModel.GetLocationByCityNameCommand.Execute(null);

            this.AssertSuccessfulResponse(viewModel.Location);
        }

        /// <summary>Tests that the current weather can be retrieved by City Coordinates.</summary>
        [TestMethod]
        [TestCategory(Consts.CategoryCurrentWeather)]
        public void Current_Weather_By_City_Coordinates_Returns_Expected_Data()
        {
            var viewModel = new TheOnlyWeatherAppEver.ViewModels.SearchLocationViewModel();
            viewModel.LatitudeInput = Consts.Latitude;
            viewModel.LongitudeInput = Consts.Longitude;
            viewModel.GetLocationByCoordinatesCommand.Execute(null);

            this.AssertSuccessfulResponse(viewModel.Location);
        }

        /// <summary>Tests that the current weather with a dodgy City ID will not return unexpected values.</summary>
        [TestMethod]
        [TestCategory(Consts.CategoryCurrentWeather)]
        public void Current_Weather_By_Incorrect_City_Id_Returns_Expected_Data()
        {
            var viewModel = new TheOnlyWeatherAppEver.ViewModels.SearchLocationViewModel();
            viewModel.CityIdInput = "-123456789";

            viewModel.GetLocationByCityIdCommand.Execute(null);

            this.AssertFailedResponse(viewModel.Location);
        }

        /// <summary>Tests that the current weather with a dodgy City Name will not return unexpected values.</summary>
        [TestMethod]
        [TestCategory(Consts.CategoryCurrentWeather)]
        public void Current_Weather_For_Incorrect_City_Name_Returns_Expected_Data()
        {
            var viewModel = new TheOnlyWeatherAppEver.ViewModels.SearchLocationViewModel();
            viewModel.CityNameInput = "ThisIsNotACity123";
            viewModel.GetLocationByCityNameCommand.Execute(null);

            this.AssertFailedResponse(viewModel.Location);
        }

        /// <summary>Tests that the current weather with a dodgy City Coordinates will not return unexpected values.</summary>
        [TestMethod]
        [TestCategory(Consts.CategoryCurrentWeather)]
        public void Current_Weather_By_City_Incorrect_Coordinates_Returns_Expected_Data()
        {
            var viewModel = new TheOnlyWeatherAppEver.ViewModels.SearchLocationViewModel();
            viewModel.LatitudeInput = "-9999.9999";
            viewModel.LongitudeInput = "-9999.9999";
            viewModel.GetLocationByCoordinatesCommand.Execute(null);

            this.AssertFailedResponse(viewModel.Location);
        }

        /// <summary>Tests that the current weather can be retrieved by auto detecting device location.</summary>
        /// <remarks>Obviously the result of this test will be affected by the device's location, so it's hard to ensure the data returned is correct. For now I'm simply checking for populated return data.</remarks>
        [TestMethod]
        [TestCategory(Consts.CategoryCurrentWeather)]
        public void Auto_Detect_By_IP_Returns_Expected_Data()
        {
            var viewModel = new TheOnlyWeatherAppEver.ViewModels.SearchLocationViewModel();
            viewModel.GetLocationByAutoDetectCommand.Execute(null);
            var location = viewModel.Location;
            Assert.IsNotNull(location);
            Assert.IsNotNull(location.Id);
            Assert.IsNotNull(location.CityName);
            Assert.IsNotNull(location.LocationData.CountryCode);
            Assert.IsNotNull(location.Coordinates.Latitude);
            Assert.IsNotNull(location.Coordinates.Longitude);
        }

        private void AssertSuccessfulResponse(TheOnlyWeatherAppEver.Models.Location location)
        {
            Assert.IsNotNull(location);
            Assert.AreEqual(Convert.ToInt32(Consts.CityId), location.Id);
            Assert.AreEqual(Consts.CityName, location.CityName);
            Assert.AreEqual(Consts.CountryCode, location.LocationData.CountryCode);
            Assert.AreEqual(Convert.ToDouble(Consts.Latitude), location.Coordinates.Latitude);
            Assert.AreEqual(Convert.ToDouble(Consts.Longitude), location.Coordinates.Longitude);
        }

        private void AssertFailedResponse(TheOnlyWeatherAppEver.Models.Location location)
        {
            if (location == null)
                return;

            Assert.AreNotEqual(Consts.CityId, location.Id);
            Assert.AreNotEqual(Consts.CityName, location.CityName);
            Assert.AreNotEqual(Consts.Latitude, location.Coordinates.Latitude);
            Assert.AreNotEqual(Consts.Longitude, location.Coordinates.Longitude);
        }
    }
}
