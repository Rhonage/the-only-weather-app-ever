﻿using System.ComponentModel;

namespace TheOnlyWeatherAppEver.ViewModels
{
    /// <summary>
    /// The base class for view models.
    /// </summary>
    internal class ViewModelBase : INotifyPropertyChanged
    {
        /// <summary>The event for when a property has been changed.</summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>Fire the property changed event.</summary>
        /// <param name="propertyName">The supplied property name.</param>
        protected virtual void RaisePropertyChangedEvent(string propertyName)
        {
            var handler = this.PropertyChanged;

            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
