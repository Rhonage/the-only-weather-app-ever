﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using TheOnlyWeatherAppEver.Commands;
using TheOnlyWeatherAppEver.Models;

namespace TheOnlyWeatherAppEver.ViewModels
{
    /// <summary>
    /// The model for the user control of the same name.
    /// </summary>
    internal class SearchLocationViewModel : ViewModelBase
    {
        private const string LocationKey = "Location";
        private const string WeatherDataVisibilityKey = "WeatherDataVisibility";
        private const string WeatherTypeIconKey = "WeatherTypeIcon";

        #region Property Bindings
        /// <summary>Location, which the UI is bound to.</summary>
        public Location Location
        {
            get { return this._location; }
            set
            {
                this._location = value;
                this.RaisePropertyChangedEvent(SearchLocationViewModel.LocationKey);
            }
        }

        /// <summary>Visibility for the weather data container, which the UI is bound to.</summary>
        public Visibility WeatherDataVisibility
        {
            get { return this._weatherDataVisibility; }
            set
            {
                this._weatherDataVisibility = value;
                this.RaisePropertyChangedEvent(SearchLocationViewModel.WeatherDataVisibilityKey);
            }
        }

        /// <summary>Weather type icon, which the UI is bound to.</summary>
        public BitmapSource WeatherTypeIcon
        {
            get
            {
                return this._weatherTypeIcon;
            }
            set
            {
                this._weatherTypeIcon = value;
                this.RaisePropertyChangedEvent(SearchLocationViewModel.WeatherTypeIconKey);
            }
        }

        /// <summary>Input for the City ID. Only used for Unit Testing at this stage.</summary>
        public string CityIdInput { get; set; }

        /// <summary>Input for the City Name, which the UI is bound to.</summary>
        public string CityNameInput { get; set; }

        /// <summary>Input for the Country Code, which the UI is bound to.</summary>
        public string CountryCodeInput { get; set; }

        /// <summary>Input for the Latitude Input, which the UI is bound to.</summary>
        public string LatitudeInput { get; set; }

        /// <summary>Input for the Longitude Input, which the UI is bound to.</summary>
        public string LongitudeInput { get; set; }
        #endregion

        #region Commands
        /// <summary>Command for retriving a location by City Name.</summary>
        public ICommand GetLocationByCityNameCommand { get; private set; }

        /// <summary>Command for retriving a location by City Coordinates.</summary>
        public ICommand GetLocationByCoordinatesCommand { get; private set; }

        /// <summary>Command for retriving a location by auto detected via external IP address.</summary>
        public ICommand GetLocationByAutoDetectCommand { get; private set; }

        /// <summary>Command for retriving a location by City ID. Currently only used for Unit Testing.</summary>
        public ICommand GetLocationByCityIdCommand { get; private set; }
        #endregion

        #region Backing Fields
        private Location _location;
        private BitmapSource _weatherTypeIcon;
        private Visibility _weatherDataVisibility;
        #endregion

        /// <summary>Creates an instance of a <see cref="SearchLocationViewModel"/>.</summary>
        public SearchLocationViewModel()
        {
            // Rohan's note: These are the input values for Timaru, and are here purely for convience while front end testing.
            this.CityNameInput = "Timaru";
            this.CountryCodeInput = "NZ";
            this.LatitudeInput = "-44.4";
            this.LongitudeInput = "171.25";

            // Set the default values.
            this.WeatherDataVisibility = Visibility.Hidden;
            this.GetLocationByCityNameCommand = new RelayCommand<Location>(e => { this.SetLocationByCityName(); });
            this.GetLocationByCoordinatesCommand = new RelayCommand<Location>(e => { this.SetLocationByCoordinates(); });
            this.GetLocationByAutoDetectCommand = new RelayCommand<Location>(e => { this.SetLocationByAutoDetect(); });
            this.GetLocationByCityIdCommand = new RelayCommand<Location>(e => { this.SetLocationByCityId(); });
        }

        /// <summary>Fire the property changed event.</summary>
        /// <param name="propertyName">The supplied property name.</param>
        protected override void RaisePropertyChangedEvent(string propertyName)
        {
            base.RaisePropertyChangedEvent(propertyName);

            // Rohan's note: I'm using the property changed event to update the weather elements on the front end (since we will have a new Location at this point).
            if (propertyName == "Location" && this.Location != null)
            {
                this.WeatherDataVisibility = Visibility.Visible;
                this.WeatherTypeIcon = new BitmapImage(this.Location.WeatherTypes.First().IconUri);
            }
        }

        private void SetLocationByCoordinates()
        {
            this.WeatherDataVisibility = Visibility.Hidden;
            var consumer = new Consumers.OpenWeatherCurrent();
            double latitude;
            double longitude;
            var success = true;

            success = Double.TryParse(this.LatitudeInput.Trim(), out latitude);
            success = Double.TryParse(this.LongitudeInput.Trim(), out longitude);

            if (!success || latitude == 0 || longitude == 0)
            {
                this.DisplayError("Could not validation latitude/longitude inputs. Please ensure Latitude and Longitude inputs are valid numerical values.");
                return;
            }

            var location = consumer.GetLocationByCoordinates(latitude, longitude);

            if (location == null)
            {
                this.DisplayError(String.Format("Could not find location with latitude '{0}' and longitude '{1}'.", latitude, longitude));
                return;
            }

            this.Location = location;
        }

        private void SetLocationByCityName()
        {
            this.WeatherDataVisibility = Visibility.Hidden;
            var consumer = new Consumers.OpenWeatherCurrent();
            var cityNameInput = this.CityNameInput.Trim();
            var countryCodeInput = this.CountryCodeInput.Trim();
            var location = consumer.GetLocationByName(cityNameInput, countryCodeInput);

            // Rohan's note: I've added very, very basic validation. This would generally be a mix of front end validation and back end checks to prevent injection attacks, etc.
            if (location == null || location.CityName != cityNameInput)
            {
                this.DisplayError(String.Format("Location with City Name '{0}' could not be found.", cityNameInput));
                return;
            }

            this.Location = location;
        }

        /// <remarks>Not used in the view. Here for unit testing, and possible future implementation.</remarks>
        private void SetLocationByCityId()
        {
            var consumer = new Consumers.OpenWeatherCurrent();
            int cityId;
            var success = Int32.TryParse(this.CityIdInput.Trim(), out cityId);

            if (!success)
                return;

            this.Location = consumer.GetLocationById(cityId);
        }

        /// <remarks>Not used in the view. Here for unit testing, and possible future implementation.</remarks>
        private void SetLocationByAutoDetect()
        {
            var geoIpConsumer = new Consumers.FreeGeoIPConsumer();
            var openWeatherConsumer = new Consumers.OpenWeatherCurrent();
            var geoLocation = geoIpConsumer.GetLocationData();
            var location = openWeatherConsumer.GetLocationByName(geoLocation.City, geoLocation.CountryCode);

            if (location == null)
            {
                this.DisplayError("Could not auto detect location.");
                return;
            }

            this.Location = location;
        }

        private void DisplayError(string message)
        {
            MessageBox.Show(message, "Oops - Something Bad Happened");
        }
    }
}
