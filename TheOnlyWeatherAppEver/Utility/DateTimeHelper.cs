﻿using System;

namespace TheOnlyWeatherAppEver.Utility
{
    /// <summary>
    /// Provides utility methods for the <see cref="DateTime"/> class.
    /// </summary>
    internal static class DateTimeHelper
    {
        /// <summary>Converts a Unix time to local time.</summary>
        /// <param name="seconds">Unix time in seconds.</param>
        internal static DateTime FromUnixTimeSeconds(long seconds)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)
                .AddSeconds(seconds).ToLocalTime();

            return dtDateTime;
        }
        
    }
}
