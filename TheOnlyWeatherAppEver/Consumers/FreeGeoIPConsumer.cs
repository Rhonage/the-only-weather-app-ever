﻿using RestConsumer;
using System;
using System.Threading.Tasks;
using TheOnlyWeatherAppEver.Models;

namespace TheOnlyWeatherAppEver.Consumers
{
    /// <summary>
    /// The RestConsumer for retrieving the external IP of the device.
    /// </summary>
    internal class FreeGeoIPConsumer : ConsumerBase
    {
        /// <summary>The URI endpoint for the RESTful API call.</summary>
        /// <remarks>'{0}' is form IP/hostname.</remarks>
        protected override string BaseUri { get { return "http://freegeoip.net/json/{0}"; } }

        /// <summary>Retrieves the <see cref="IPGeoLocation"/> data for the device.</summary>
        /// <returns></returns>
        public IPGeoLocation GetLocationData()
        {
            return this.GetDeviceLocationForIPAddress().Result;
        }

        private async Task<IPGeoLocation> GetDeviceLocationForIPAddress()
        {
            var externalIPAddress = await this.GetExternalIPAddress().ConfigureAwait(false);

            return await this.GetGeoLocation(externalIPAddress).ConfigureAwait(false);
        }

        private async Task<string> GetExternalIPAddress()
        {
            var uri = "https://api.ipify.org/";
            var request = new RestRequest(uri);
            var response = await this.ExecuteAsync<string>(request).ConfigureAwait(false);
            
            return response.Data;
        }

        private async Task<IPGeoLocation> GetGeoLocation(string externalIPAddress)
        {
            var uri = String.Format(this.BaseUri, externalIPAddress);
            var request = new RestRequest(uri);
            var response = await this.ExecuteAsync<IPGeoLocation>(request).ConfigureAwait(false);

            return response.Data;
        }
    }
}
