﻿using RestConsumer;
using System;
using System.Threading.Tasks;
using TheOnlyWeatherAppEver.Models;

namespace TheOnlyWeatherAppEver.Consumers
{
    /// <summary>
    /// The base class for OpenWeatherAPI consumers.
    /// </summary>
    internal class OpenWeatherBase : ConsumerBase
    {
        /// <summary>The unique application ID, used as validation to make the API calls.</summary>
        private const string AppId = "365225ef1ada5064d118013f3e6c0137";

        /// <summary>The base URI of the OpenWeatherAPI platform.</summary>
        protected override string BaseUri { get { return "http://api.openweathermap.org/data/2.5/{0}{1}&units=" + Units.Metric.ToString() + "&appid=" + OpenWeatherBase.AppId; } }
    }

    /// <summary>
    /// The interface that all Open Weather API consumers must conform to.
    /// </summary>
    internal interface IOpenWeatherConsumer
    {
        /// <summary>Retrieve current weather data on a location by coordinates.</summary>
        /// <param name="latitude">The latitude value for a location.</param>
        /// <param name="longitude">The longitude value for a location.</param>
        Location GetLocationById(int id);

        /// <summary>Retrieves current weather data on a location by location name.</summary>
        /// <param name="name">The name of the location.</param>
        /// <param name="countryCode">The country code that the location resides in.</param>
        Location GetLocationByName(string name, string countryCode = null);

        /// <summary>Retrieves current weather data on a location by coordinates.</summary>
        /// <param name="latitude">The latitude value for a location.</param>
        /// <param name="longitude">The longitude value for a location.</param>
        Location GetLocationByCoordinates(double latitude, double longitude);
    }

    /// <summary>
    /// The units of measurement, used for an API call.
    /// </summary>
    internal enum Units { Metric, Imperial, Standard }

    /// <summary>
    /// The consumer used to retrieve data about the current weather.
    /// </summary>
    internal class OpenWeatherCurrent : OpenWeatherBase, IOpenWeatherConsumer
    {
        /// <summary>Retrieves current weather data on a location by coordinates.</summary>
        /// <param name="latitude">The latitude value for a location.</param>
        /// <param name="longitude">The longitude value for a location.</param>
        public Location GetLocationByCoordinates(double latitude, double longitude)
        {
            return this.GetLocationWeatherAsync(null, null, null, latitude, longitude).Result;
        }

        /// <summary>Retrieves current weather data on a location by location ID.</summary>
        /// <param name="id">The location ID.</param>
        public Location GetLocationById(int id)
        {
            if (id <= 0)
                return null;

            return this.GetLocationWeatherAsync(id, null, null, null, null).Result;
        }

        /// <summary>Retrieves current weather data on a location by location name.</summary>
        /// <param name="name">The name of the location.</param>
        /// <param name="countryCode">The country code that the location resides in.</param>
        public Location GetLocationByName(string name, string countryCode = null)
        {
            if (String.IsNullOrEmpty(name))
                return null;

            return this.GetLocationWeatherAsync(null, name, countryCode, null, null).Result;
        }

        private async Task<Location> GetLocationWeatherAsync(int? id, string name, string countryCode, double? latitude, double? longitude)
        {
            // Form the Uri string based on the supplied data
            string uri = String.Format(this.BaseUri, "weather", "{0}");

            if (id != null)
                uri = String.Format(uri, "?id=" + id);
            else if (name != null)
                uri = String.Format(uri, "?q=" + name + (countryCode != null ? "," + countryCode : null));
            else if (latitude != null && longitude != null)
                uri = String.Format(uri, "?lat=" + latitude + "&lon=" + longitude);

            // Perform the execution on the consumer
            var request = new RestRequest(uri);
            var response = await this.ExecuteAsync<Location>(request).ConfigureAwait(false);
            var location = response.Data;

            return location;
        }
    }
}
