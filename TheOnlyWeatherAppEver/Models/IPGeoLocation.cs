﻿using System.Runtime.Serialization;

namespace TheOnlyWeatherAppEver.Models
{
    /// <summary>
    /// The model to deserialize to for a <see cref="Consumers.FreeGeoIPConsumer"/> response.
    /// </summary>
    [DataContract]
    internal class IPGeoLocation
    {
        /// <summary>The name of the city of the location.</summary>
        [DataMember(Name = "City")]
        public string City { get; set; }

        /// <summary>The country code of the location.</summary>
        [DataMember(Name = "Country_Code")]
        public string CountryCode { get; set; }

        /// <summary>The name of the country of the location.</summary>
        [DataMember(Name = "Country_Name")]
        public string CountryName { get; set; }

        /// <summary>The external IP Address of the location.</summary>
        [DataMember(Name = "Ip")]
        public string IPAddress { get; set; }

        /// <summary>The latitude value of the location.</summary>
        [DataMember(Name = "Latitude")]
        public double Latitude { get; set; }

        /// <summary>The longitude value of the location.</summary>
        [DataMember(Name = "Longitude")]
        public double Longitude { get; set; }

        /// <summary>The metro code of the location.</summary>
        [DataMember(Name = "Metro_Code")]
        public string MetroCode { get; set; }

        /// <summary>The regional code of the location.</summary>
        [DataMember(Name = "Region_Code")]
        public string RegionCode { get; set; }

        /// <summary>The regional name of the location.</summary>
        [DataMember(Name = "Region_Name")]
        public string RegionName { get; set; }

        /// <summary>The time zone that the location resides in.</summary>
        [DataMember(Name = "Time_Zone")]
        public string TimeZone { get; set; }

        /// <summary>The zip code of the location.</summary>
        [DataMember(Name = "Zip_Code")]
        public string ZipCode { get; set; }
    }
}
