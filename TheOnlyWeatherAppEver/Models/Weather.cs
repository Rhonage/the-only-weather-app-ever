﻿using System;
using System.Runtime.Serialization;

namespace TheOnlyWeatherAppEver.Models
{
    /// <summary>Model for current weather data.</summary>
    [DataContract(Name = "Weather")]
    internal class WeatherType
    {
        /// <summary>The unique identifier for the weather condition.</summary>
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>The name of the weather condition (rain, snow, etc).</summary>
        [DataMember(Name = "Main")]
        public string Name { get; set; }

        /// <summary>The description for the weather condition.<summary>
        [DataMember(Name = "Description")]
        public string Description { get; set; }

        /// <summary>The unique identifier for the weather icon.</summary>
        [DataMember(Name = "Icon")]
        public string IconName { get; set; }

        /// <summary>The local path of the icon PNG file.</summary>
        public Uri IconUri { get { return new Uri(String.Format("/TheOnlyWeatherAppEver;component/WeatherTypeIcons/{0}.png", this.IconName), UriKind.Relative); } }

        /// <summary>The URL of the icon PNG file, as provided by OpenWeather API.</summary>
        /// <remarks>Unfortunately, these are too small to be any use.</remarks>
        public string IconOpenWeatherUrl { get { return String.Format("http://openweathermap.org/img/w/{0}.png", this.IconName); } }
    }

    /// <summary>
    /// The main useful weather data.
    /// </summary>
    [DataContract(Name = "Main")]
    internal class WeatherData
    {
        /// <summary>The current temperature.</summary>
        [DataMember(Name = "Temp")]
        public float Temperature { get; set; }

        /// <summary>The current atmospheric pressure (default is at sea level).</summary>
        [DataMember(Name = "Pressure")]
        public float Pressure { get; set; }

        /// <summary>The current humidity.</summary>
        [DataMember(Name = "Humidity")]
        public float Humidity { get; set; }

        /// <summary>The current minimum temperature for a location. The is a deviation from <see cref="Temperature"/> in that larger cities may have a variance.</summary>
        [DataMember(Name = "Temp_Min")]
        public float TemperatureMin { get; set; }

        /// <summary>The current maximum temperature for a location. The is a deviation from <see cref="Temperature"/> in that larger cities may have a variance.</summary>
        [DataMember(Name = "Temp_Max")]
        public float TemperatureMax { get; set; }
    }
}
