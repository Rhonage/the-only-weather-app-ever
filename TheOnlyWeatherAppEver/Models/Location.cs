﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using TheOnlyWeatherAppEver.Utility;

namespace TheOnlyWeatherAppEver.Models
{
    /// <summary>
    /// The location data for the current weather.
    /// </summary>
    [DataContract]
    internal class Location
    {
        /// <summary>The unique identifier for the City/Town for the <see cref="Location"/>.</summary>
        [DataMember(Name = "Id")]
        public int Id { get; set; }

        /// <summary>The name of the City/Town for the <see cref="Location"/>.</summary>
        [DataMember(Name = "Name")]
        public string CityName { get; set; }

        /// <summary>The coordinates of the <see cref="Location"/>, represented in Latitude and Longitude.</summary>
        [DataMember(Name = "Coord")]
        public GeoLocation Coordinates { get; set; }

        /// <summary>Weather information for the <see cref="Location"/>.</summary>
        [DataMember(Name = "Weather")]
        public List<WeatherType> WeatherTypes { get; set; }

        /// <summary></summary>
        [DataMember(Name = "Main")]
        public WeatherData WeatherData { get; set; }

        /// <summary>Wind speed and direction.</summary>
        [DataMember(Name = "Wind")]
        public Wind Wind { get; set; }
        /// <summary>Amount of cloud coverage as a percentage.</summary>
        [DataMember(Name = "Clouds")]
        public Cloudiness Cloudiness { get; set; }

        /// <summary>The time that the weather data was last updated (in local time).</summary>
        public DateTime LastUpdated { get { return DateTimeHelper.FromUnixTimeSeconds(this._lastUpdatedUnixSeconds); } }

        /// <summary>The data for the physical location.</summary>
        [DataMember(Name = "Sys")]
        public LocationData LocationData { get; set; }

        /// <summary>The time that the weather data was last updated (in Unix seconds).</summary>
        [DataMember(Name = "dt")]
        private long _lastUpdatedUnixSeconds;
    }

    /// <summary>
    /// The coordinates of the <see cref="Location"/>, represented in Latitude and Longitude.
    /// </summary>
    [DataContract(Name = "Coord")]
    internal struct GeoLocation
    {
        /// <summary>The latitudinal value of the geo location.</summary>
        [DataMember(Name = "Lat")]
        public double Latitude { get; set; }

        /// <summary>The longitudinal value of the geo location</summary>
        [DataMember(Name = "Lon")]
        public double Longitude { get; set; }
    }

    /// <summary>
    /// Further information for the physical location.
    /// </summary>
    [DataContract(Name = "Sys")]
    internal struct LocationData
    {
        /// <summary>The sunrise time for the physical location (in local time).</summary>
        public DateTime Sunrise { get { return DateTimeHelper.FromUnixTimeSeconds(this._sunriseUnixSeconds); } }

        /// <summary>The sunset time for the physical location (in local time).</summary>
        public DateTime Sunset { get { return DateTimeHelper.FromUnixTimeSeconds(this._sunsetUnixSeconds); } }

        /// <summary>The country code for the location.</summary>
        [DataMember(Name = "Country")]
        public string CountryCode { get; set; }

        /// <summary>The sunrise time for the physical location (in Unix seconds).</summary>
        [DataMember(Name = "Sunrise")]
        private long _sunriseUnixSeconds;

        /// <summary>The sunset time for the physical location (in Unix seconds).</summary>
        [DataMember(Name = "Sunset")]
        private long _sunsetUnixSeconds;
    }

    /// <summary>
    /// Model for wind data.
    /// </summary>
    [DataContract(Name = "Wind")]
    internal struct Wind
    {
        /// <summary>Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.</summary>
        [DataMember(Name = "Speed")]
        public float Speed { get; set; }
        /// <summary>Wind direction, degrees (meteorological).</summary>
        [DataMember(Name = "Deg")]
        public float Degrees { get; set; }
    }

    [DataContract(Name = "Clouds")]
    internal struct Cloudiness
    {
        [DataMember(Name = "All")]
        public float Percentage { get; set; }
    }
}