﻿using System;
using System.Windows.Input;

namespace TheOnlyWeatherAppEver.Commands
{
    /// <summary>
    /// The command used for relaying an instruction (generically from the UI, or unit tests).
    /// </summary>
    /// <typeparam name="T">The supplied generic type.</typeparam>
    internal class RelayCommand<T> : ICommand
    {
        #region Fields
        private readonly Action<T> _execute = null;
        private readonly Predicate<T> _canExecute = null;
        #endregion

        #region Constructors
        /// <summary>Creates a new command that can always execute.</summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<T> execute)
            : this(execute, null)
        {
        }

        /// <summary>Creates a new command with conditional execution.</summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<T> execute, Predicate<T> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            _execute = execute;
            _canExecute = canExecute;
        }
        #endregion

        #region ICommand Members
        /// <summary>Performs a check to evaluate whether a command can be executed or not.</summary>
        /// <param name="parameter">Data used by the command, but can be null if there is no data to supply.</param>
        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute((T)parameter);
        }

        /// <summary>Fires when <see cref="_canExecute"/> is changed.</summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (_canExecute != null)
                    CommandManager.RequerySuggested += value;
            }
            remove
            {
                if (_canExecute != null)
                    CommandManager.RequerySuggested -= value;
            }
        }

        /// <summary>Defines the method to be called when a command is invoked.</summary>
        /// <param name="parameter">Data used by the command, but can be null if there is no data to supply.</param>
        public void Execute(object parameter)
        {
            _execute((T)parameter);
        }
        #endregion
    }
}