# README #

Hi James / Matt,

As required, all code has been written in C#. For a front end I decided to go with WPF and a MVVM pattern to keep it clean and maintain separation of concerns. 

I chose WPF because it's familiar enough to me to not get bogged down, but also to use this oppourtunity to learn more about it, and more or less do some up-skilling in a WPF environment.

I have also included an executable in the case that you decide you'd like to jump straight into the front end.

All code is documented to the best of my ability, and I have intentionally kept it clean and simple to follow - but please ask me any questions if there's anything unclear, and I'll be happy to discuss.


**Build Instructions**

Should be pretty straight forward:

1. Open the solution file in Visual Studio (any version that targets .NET 4.5.2 and above - I used VS2017).

2. You may have to restore NuGet packages, but VS will probably be smart enough to do that for you when you attempt to build.

3. Run the application.

You should see the following:

![cover.png](https://bitbucket.org/repo/7EErEkx/images/1596515476-cover.png)


As you can tell, I'm not a front end designer - but I get by.

---


** Things To Note **

From my understanding this project was more of a proficiency test (unless of course P&L would like to expand into throwaway-weather-app-territory...?), so there's a couple of things I feel I should mention:

- Due to my limited time available, I decided to focus solely on the current weather data (https://openweathermap.org/current). You will notice my models are very close to mirroring the response JSON from the API calls. In a real world scenario, I would separate my models out into structures such as "Common" (containing datatypes used by multiple consumer clients), "CurrentWeather", "ForecastWeather", etc. 

- There's a few "best practices" that I unfortunately haven't had the time to implement, such as separating strings out into their own file for localisation, etc. Since this is a throwaway project, I didn't consider this to be a priority.

- Please note that minimum temperature (the light blue square) and maximum temperature (the light red square) are the minimum and maximum temperatures for the current temperature across a geolocation. 99% of the time (for the smallish New Zealand cities) all 3 temperature readouts will be the same. Try searching for "New York" or something similar and you'll see how the temperature can range across geographically larger cities.

- James, you had mentioned that binaries aren't necessary. This lead me to believe that you are more interested in the source code than the front end. Because of this I have purposely spent a small amount of time implementing front end input validation (as you would expect to see in production code) - so hopefully that doesn't knock a few points off ;).

**TheOnlyWeatherAppEver Components**

I have separated the codebase out into 3 distinct projects. RestConsumer (the layer to actually perform the REST API calls, without caring about what is calling it, or what it expects to get back), TheOnlyWeatherAppEver (the WPF application itself, containing all views, view models, business logic, and models), and TheOnlyWeatherAppEver.Tests (contains some basic unit tests for the overall application, by hooking into the view models and calling the appropriate methods).

**Dependencies**

- Newtonsoft.Json is used to deserialize JSON data formats from a server response.

- MahApps.Metro is used to make it look pretty-ish.

That's it. I wrote the rest myself.

Cheers - looking forward to your feedback.


Rohan